use std::sync::mpsc;
use std::thread;
use std::time::Duration;

const N: i32 = 5; // number of threads

// You should modify main() to spawn threads and communicate using channels
fn main() {
    let (tx, rx) = mpsc::channel();
    let mut children = Vec::new();

    for i in 0..N{
        let tx = mpsc::Sender::clone(&tx);
        children.push(thread::spawn(move || {
            tx.send(format!("Hello from thread {}", i)).unwrap();
            thread::sleep(Duration::from_secs(1))
        }));
    }

    for _ in 0..N{
        let res = rx.recv().expect("Error receiving message");
        println!("Got: {}", res);
    }

    for child in children{
        let _res = child.join();
    }
}
